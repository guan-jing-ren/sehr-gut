#pragma once

#include "../gut/type.hpp"

#include <dirent.h>
#include <dlfcn.h>
#include <glob.h>
#include <grp.h>
#include <iconv.h>
#include <mqueue.h>
#include <net/if.h>
#include <netdb.h>
#include <pthread.h>
#include <pwd.h>
#include <semaphore.h>
#include <spawn.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <syslog.h>
#include <unistd.h>
#include <utmpx.h>
#include <wordexp.h>

#include <cstdio>
#include <cstdlib>

namespace gut {

template <auto ResourceCleanup, auto InvalidResource> struct resource {
  using resource_type = decltype(InvalidResource);
  constexpr static decltype(auto) resource_cleanup_v() {
    return ResourceCleanup;
  }
  constexpr static decltype(auto) resource_type_v() {
    return type_v<resource_type>;
  }
  constexpr static decltype(auto) invalid_resource_v() {
    return InvalidResource;
  }

  constexpr resource() = default;
  constexpr resource(const resource &) = delete;
  constexpr resource(resource &&that)
      : m_resource([&that](auto that_resource) {
          that.m_resource = invalid_resource_v();
          return that_resource;
        }(that.m_resource)) {}
  constexpr resource &operator=(const resource &) = delete;
  constexpr resource &operator=(resource &&that) {
    m_resource = that.m_resource;
    that.m_resource = invalid_resource_v();
    return *this;
  }
  ~resource() { reset(); }

  bool valid() const { return m_resource != invalid_resource_v(); }

  constexpr void reset() {
    if (m_resource != invalid_resource_v()) {
      resource_cleanup_v()(m_resource);
      m_resource = invalid_resource_v();
    }
  }

protected:
  resource_type m_resource = invalid_resource_v();
};

template <typename ValueType>
struct memory
    : public resource<static_cast<void (*)(ValueType *)>(operator delete),
                      static_cast<ValueType *>(nullptr)> {};
template <typename ValueType>
struct memory<ValueType[]>
    : public resource<static_cast<void (*)(ValueType[])>(operator delete[]),
                      static_cast<ValueType[]>(nullptr)> {};
template <>
struct memory<void> : public resource<std::free, static_cast<void *>(nullptr)> {
};
struct stream
    : public resource<std::fclose, static_cast<std::FILE *>(nullptr)> {};
struct descriptor : public resource<close, static_cast<int>(-1)> {};
struct directory : public resource<closedir, static_cast<DIR *>(nullptr)> {
  constexpr directory(const char *name);
  ~directory();
  constexpr operator bool() const;

  constexpr const dirent *operator->() const;
  constexpr const dirent &operator*() const;

  directory &operator++();

private:
  dirent *m_current = nullptr;
};
template <auto VoidFunc>
constexpr decltype(auto) void_destructor(decltype(VoidFunc)) {
  return VoidFunc();
}
struct system_log : public resource<void_destructor<closelog>, closelog> {};
struct symbol_table : public resource<dlclose, static_cast<void *>(nullptr)> {};
struct conversion
    : public resource<iconv_close, static_cast<iconv_t>(nullptr)> {};
struct message_queue : public resource<mq_close, static_cast<mqd_t>(-1)> {};
struct pipe_stream
    : public resource<pclose, static_cast<std::FILE *>(nullptr)> {};
struct named_semaphore
    : public resource<sem_close, static_cast<sem_t *>(nullptr)> {};
struct unnamed_semaphore
    : public resource<sem_destroy, static_cast<sem_t *>(nullptr)> {};
struct locked_semaphore
    : public resource<sem_post, static_cast<sem_t *>(nullptr)> {};
struct address_info
    : public resource<freeaddrinfo, static_cast<addrinfo *>(nullptr)> {
  constexpr address_info(const char *node, const char *service);
  ~address_info();

  constexpr operator bool() const;

  constexpr const addrinfo *operator->() const;
  constexpr const addrinfo &operator*() const;

  constexpr address_info &operator++();

private:
  addrinfo *m_current = nullptr;
};
struct file_glob : public resource<globfree, static_cast<glob_t *>(nullptr)> {
  constexpr file_glob(const char *pattern);

  constexpr decltype(auto) count() const;
  constexpr operator bool() const;

  constexpr const char *const *begin() const;
  constexpr const char *const *end() const;

private:
  glob_t m_glob{};
};
struct word_expansion
    : public resource<wordfree, static_cast<wordexp_t *>(nullptr)> {
  constexpr word_expansion(const char *pattern);

  constexpr decltype(auto) count() const;
  constexpr operator bool() const;

  constexpr const char *const *begin() const;
  constexpr const char *const *end() const;

private:
  wordexp_t m_wordexp{};
};
struct network_interfaces
    : public resource<if_freenameindex,
                      static_cast<struct if_nameindex *>(nullptr)> {
  constexpr network_interfaces();

  constexpr operator bool() const;

  constexpr const struct if_nameindex *begin() const;
  constexpr const struct if_nameindex *end() const;

private:
  struct if_nameindex *m_end = nullptr;
};
struct spawn_attributes
    : public resource<posix_spawnattr_destroy,
                      static_cast<posix_spawnattr_t *>(nullptr)> {};
struct spawn_file_actions
    : public resource<posix_spawn_file_actions_destroy,
                      static_cast<posix_spawn_file_actions_t *>(nullptr)> {};
struct thread_attributes
    : public resource<pthread_attr_destroy,
                      static_cast<pthread_attr_t *>(nullptr)> {};
struct thread_barrier_attributes
    : public resource<pthread_barrierattr_destroy,
                      static_cast<pthread_barrierattr_t *>(nullptr)> {};
struct thread_barrier
    : public resource<pthread_barrier_destroy,
                      static_cast<pthread_barrier_t *>(nullptr)> {};
struct thread_condition_attributes
    : public resource<pthread_condattr_destroy,
                      static_cast<pthread_condattr_t *>(nullptr)> {};
struct thread_condition
    : public resource<pthread_cond_destroy,
                      static_cast<pthread_cond_t *>(nullptr)> {};
struct thread_key : public resource<pthread_key_delete,
                                    static_cast<pthread_key_t *>(nullptr)> {};
struct thread : public resource<pthread_join, static_cast<pthread_t>(-1)> {};
struct thread_mutex_attributes
    : public resource<pthread_mutexattr_destroy,
                      static_cast<pthread_mutexattr_t *>(nullptr)> {};
struct thread_mutex : public resource<pthread_mutex_destroy,
                                      static_cast<pthread_mutex_t *>(nullptr)> {
};
struct thread_mutex_lock
    : public resource<pthread_mutex_unlock,
                      static_cast<pthread_mutex_t *>(nullptr)> {};
struct thread_readwrite_mutex_attributes
    : public resource<pthread_rwlockattr_destroy,
                      static_cast<pthread_rwlockattr_t *>(nullptr)> {};
struct thread_readwrite_mutex
    : public resource<pthread_rwlock_destroy,
                      static_cast<pthread_rwlock_t *>(nullptr)> {};
struct thread_readwrite_mutex_lock
    : public resource<pthread_rwlock_unlock,
                      static_cast<pthread_rwlock_t *>(nullptr)> {};
struct thread_spin_mutex
    : public resource<pthread_spin_destroy,
                      static_cast<pthread_spinlock_t *>(nullptr)> {};
struct thread_spin_lock
    : public resource<pthread_spin_unlock,
                      static_cast<pthread_spinlock_t *>(nullptr)> {};
struct group_database : public resource<void_destructor<endgrent>, endgrent> {
  constexpr group_database();
  ~group_database();

  constexpr operator bool() const;

  constexpr const group *operator->() const;
  constexpr const group &operator*() const;

  group_database &operator++();

private:
  group *m_current = nullptr;
};
struct host_database
    : public resource<void_destructor<endhostent>, endhostent> {
  constexpr host_database();
  ~host_database();

  constexpr operator bool() const;

  constexpr const hostent *operator->() const;
  constexpr const hostent &operator*() const;

  host_database &operator++();

private:
  hostent *m_current = nullptr;
};
struct network_database
    : public resource<void_destructor<endnetent>, endnetent> {
  constexpr network_database();
  ~network_database();

  constexpr operator bool() const;

  constexpr const netent *operator->() const;
  constexpr const netent &operator*() const;

  network_database &operator++();

private:
  netent *m_current = nullptr;
};
struct protocol_database
    : public resource<void_destructor<endprotoent>, endprotoent> {
  constexpr protocol_database();
  ~protocol_database();

  constexpr operator bool() const;

  constexpr const protoent *operator->() const;
  constexpr const protoent &operator*() const;

  protocol_database &operator++();

private:
  protoent *m_current = nullptr;
};
struct user_database : public resource<void_destructor<endpwent>, endpwent> {
  constexpr user_database();
  ~user_database();

  constexpr operator bool() const;

  constexpr const passwd *operator->() const;
  constexpr const passwd &operator*() const;

  user_database &operator++();

private:
  passwd *m_current = nullptr;
};
struct service_database
    : public resource<void_destructor<endservent>, endservent> {
  constexpr service_database();
  ~service_database();

  constexpr operator bool() const;

  constexpr const servent *operator->() const;
  constexpr const servent &operator*() const;

  service_database &operator++();

private:
  servent *m_current = nullptr;
};
struct account_database
    : public resource<void_destructor<endutxent>, endutxent> {
  constexpr account_database();
  ~account_database();

  constexpr operator bool() const;

  constexpr const utmpx *operator->() const;
  constexpr const utmpx &operator*() const;

  account_database &operator++();

private:
  utmpx *m_current = nullptr;
};
struct file_lock
    : public resource<funlockfile, static_cast<std::FILE *>(nullptr)> {};
struct timer : public resource<timer_delete, static_cast<timer_t>(nullptr)> {};
struct shared_memory : public resource<shm_unlink, static_cast<int>(-1)> {};
struct mapped_memory : public resource<munmap, static_cast<void *>(nullptr)> {};
} // namespace gut

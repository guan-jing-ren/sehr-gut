CLANGHOME=$(HOME)/clang
CXX=$(CLANGHOME)/bin/clang++
CXXFLAGS=-std=c++17 -Os -g -gsplit-dwarf -Wall -Wextra -Wpedantic -Werror\
		 -fPIC -stdlib=libc++ -I . -I $(CLANGHOME)/include/c++/v1/
LXXFLAGS=-L $(CLANGHOME)/lib -Wl,-rpath=$(CLANGHOME)/lib/
# CXX=g++-8
# CXXFLAGS=-std=c++17 -Os -g -gsplit-dwarf -Wall -Wextra -Wpedantic -Werror\
		 -fno-exceptions -fno-rtti -ffreestanding\
		 -fno-plt -fno-use-cxa-atexit -fno-common -fno-merge-all-constants\
		 -fno-stack-protector
MAX_ARGS=64
OBJECTS=$(addsuffix .o, $(basename $(wildcard *.cpp)))
DOCS=gut_dependencies.svg $(addprefix gut_, $(addsuffix .html, $(basename $(wildcard *.cpp))))
EDGETOOLTIP=edge [dir=back tooltip=\"\\\\H begat \\\\T\"]
DEPTREESCRIPT='END { print "}" }\
		{\
			l = gensub(/:\#include/, "", "g");\
			l = gensub(/["<>]/, "", "g", l);\
			l = gensub(/\S+/, "\"\\0\"", "g", l);\
			l = gensub(/ /, " -> ", "g", l);\
			print gensub(/\"(\S+?)\.hpp\"/, "\\1", "g", l);\
			print gensub(/\"(\S+?)\.hpp\" -> .*/,\
						"\"\\1\" [URL=\"gut_\\1.html\" target=\"gutlibdoc\"]",\
						1, l);\
			if (match(gensub(/.* -> /, "", "g", l), /.*\.hpp/))\
				print gensub(/.*? -> \"(\S+?)\.hpp\".*/,\
							"\"\\1\" [URL=\"gut_\\1.html\" target=\"gutlibdoc\"]",\
							1, l);\
			else print gensub(/.*? -> \"([^.]+?)\".*/,\
							"\"\\1\" [URL=\"https://en.cppreference.com/w/cpp/header/\\1\" \
							target=\"gutlibdoc\"]", 1, l);\
		}'
DIJKSTRASCRIPT=| dijkstra config | awk -e '\
		{\
			print $$0;\
			if (match($$0, /cppref/))\
				getline;\
		}' | awk -e '\
		{\
			if (match($$0, /URL/))\
				s = gensub(/(.*?) \[.*/, "\\1", "g");\
			l = $$0;\
			if (!match(l, /\tdist/)) {\
				print l;\
				next;\
			}\
			getline;\
			while (!match($$0,/;/)) {\
				print $$0;\
				getline;\
			}\
			print $$0;\
			print gensub(/\tdist=(.*?)\..*/,\
						"subgraph cluster_\\1", "g", l) " {" s "}";\
		}'

.DEFAULT: sehr-gut

pp.%: Makefile
	echo $(MAX_ARGS) | awk -e '\
		BEGIN { print "#pragma once"; print "#include \"config.hpp\"" }\
		{\
			count = $$0;\
			while (count > -1) {\
				print "#define GUT_PP_WHILE_" (count) "(FUNC, ...)"\
				"GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,GUT_PP_WHILE_CALLFUNC(FUNC," ((count==0) ? "GUT_PP_FIRST" : "GUT_PP_NOTFIRST") ", " (count+1) ", __VA_ARGS__)"\
						"GUT_PP_WHILE_" (count+1) "(FUNC, GUT_PP_HEAD(__VA_ARGS__), GUT_PP_WHILE_ARGS(__VA_ARGS__)))";\
				count--;\
			}\
		}' | $(CLANGHOME)/bin/clang-format > $@

%.o: %.cpp %.hpp pp.hpp Makefile gut_dependencies.mk
	$(CXX) $(CXXFLAGS) -c $< -o $@

gut_%.html: %.o Makefile gut_dependencies.mk top.thtml bot.thtml
	- rm $@
	- awk -e '{ print gensub(/%%LIBRARY%%/, module, "g"); }' -v module="$*" top.thtml > $@
	- awk -e '/\*\//,/\/\*/{\
			l = gensub("\\&","\\&amp;","g");\
			l = gensub(">","\\&gt;","g", l);\
			l = gensub("<","\\&lt;","g", l);\
			l = gensub("--","\\&ndash;\\&ndash;","g", l);\
			print l;\
		}\
		/\/\*/,/\*\//{ print }' $*.hpp $*.cpp \
		| grep -v -P "^\s*?/\*\$$|^\s*?\*/\$$" >> $@
	- awk -e '{ print gensub(/%%LIBRARY%%/, module, "g"); }' -v module="$*" bot.thtml >> $@
	- grep -R "#include" *.hpp | grep -v "#if" | grep -v "#error" | grep -w $* |\
		awk -e 'BEGIN {\
			print "strict digraph \"" module " dependencies\" {";\
			print "$(EDGETOOLTIP)";\
			print "{ " module " [style=filled fillcolor=green]}";\
		}' -v module="$*" -e $(DEPTREESCRIPT) | dot -Gstylesheet=dep.css -Tsvg -ogut_$*.svg

%.svg: Makefile $(wildcard *.hpp)
	- grep -R "#include" *.hpp | grep -v "#if" | grep -v "#error" |\
		awk -e 'BEGIN {\
			print "strict digraph \"Das Gut Buch Tree of Knowledge\" {";\
			print "$(EDGETOOLTIP)";\
		}' -e $(DEPTREESCRIPT) | dot -Gstylesheet=dep.css -Tsvg -o$@

# gut: ../sehr-gut ../cat-gut ../gut-microbiome $(DOCS) whygpl.thtml
# 	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $@ $(LXXFLAGS)
# 	- rm ../guan-jing-ren.bitbucket.io/*
# 	- cp gut_gut.html index.html
# 	- cp *.html *.thtml *.css *.js *.svg ../guan-jing-ren.bitbucket.io/

sehr-gut: $(DOCS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $@ $(LXXFLAGS)

cat-gut: $(DOCS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $@ $(LXXFLAGS)

gut-microbiome: $(DOCS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $@ $(LXXFLAGS)

clean:
	- rm *.o *.dwo *.html *.svg gut gut_dependencies.mk

check:
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="bugprone-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="cert-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="cppcoreguidelines-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="clang-analyzer-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="hicpp-*"\
		-config="{CheckOptions: [{key: hicpp-move-const-arg.CheckTriviallyCopyableMove, value: 0},\
								 {key: hicpp-braces-around-statements.ShortStatementLines, value: 2}]}" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="misc-*,-misc-redundant-expression" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="modernize-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="performance-*"\
		-config="{CheckOptions: [{key: performance-move-const-arg.CheckTriviallyCopyableMove, value: 0}]}" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="portability-*" *.cpp &
	$(CLANGHOME)/bin/clang-tidy -quiet -checks="readability-*,\
			-readability-static-accessed-through-instance,\
			-readability-named-parameter,\
			-readability-else-after-return"\
		-config="{CheckOptions: [{key: readability-braces-around-statements.ShortStatementLines, value: 2}]}" *.cpp &

gut_dependencies.mk:
	$(CXX) -MM *.cpp > gut_dependencies.mk

include gut_dependencies.mk

../sehr-gut:
	cd .. &&\
	git clone https://bitbucket.org/guan-jing-ren/sehr-gut.git &&\
	ln gut/Makefile sehr-gut/Makefile &&\
	cd sehr-gut && make -j8 sehr-gut && cd ../gut

../cat-gut:
	cd .. &&\
	git clone https://bitbucket.org/guan-jing-ren/cat-gut.git &&\
	ln gut/Makefile cat-gut/Makefile &&\
	cd cat-gut && make -j8 cat-gut && cd ../gut

../gut-microbiome:
	cd .. &&\
	git clone https://bitbucket.org/guan-jing-ren/gut-microbiome.git &&\
	ln gut/Makefile gut-microbiome/Makefile &&\
	cd gut-microbiome && make -j8 gut-microbiome && cd ../gut

# README #

The Supplementary Extensions and Helpers Repository for Grand Unified Toolkit of Generally Useful Things.

Licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html), or purchase a renewable or perpetual linking exception.
Purchasing a linking exception only means your work will not be considered a derivative work of this library.
Any modifications to your copy of the actual library itself must still conform to GPLv3.
Modifying the library to allow circumventing the licence is forbidden, even in the case of a purchased linking exception.

Selling licence exceptions has been blessed by [Richard Stallman himself](https://www.fsf.org/blogs/rms/selling-exceptions).

Support may also be purchased separately.

### What is this repository for? ###

* Quick summary
	- A library built on top of GUT to provide higher level runtime abstractions.
* Version
	- Rolling release.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	- `make`
	- `#include "..."`
* Configuration
	- `-Wall -Wextra -Wpedantic -Werror -std=c++<latest standard>`.
* Dependencies
	- C++ compiler for the most up-to-date published C++ standard.
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
	- No bikeshedding.
		- All design-style is driven by clear, intended usage, and not general notions of "best practice" or "design pattern" or "idiomatic".
	- Use clang-format LLVM default settings.
	- Compiles at highest warning-as-error levels and use static_asserts.
	- Keep macros to a minimum
		- Preferably defined with `-D` compiler flag, with a reasonable default `#ifndef`, and immediately defined as a `constexpr` variable in source.
		- If implementing introspection, try only to get the information needed once, store in some `constexpr` object, then use said object from thereon in.
* Other guidelines
	- Aim for use as a freestanding library for high level programming.
	- Use the most up-to-date published C++ standard, with an eye to incorporate upcoming standard features designed for better compile-time/usability.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
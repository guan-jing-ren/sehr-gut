#include "sehr-gut.hpp"

#include "resource.hpp"

#include <iostream>

using namespace gut;
using namespace std;
int main() {
  group_database gdb;
  while (gdb) {
    cout << "Group id: " << gdb->gr_gid << ", name: " << gdb->gr_name << '\n';
    cout << "Members:";
    for (auto mem = gdb->gr_mem; !*mem; ++mem)
      cout << ' ' << *mem;
    cout << '\n';
  }
  return 0;
}

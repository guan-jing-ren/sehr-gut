#include "../gut/bitfield.hpp"
#include "../gut/fstring.hpp"

#include <termios.h>

namespace gut {
namespace ecma48 {
struct bit7 : public bitfield<7, 4, 0> {
  using base_type = bitfield<7, 4, 0>;
  constexpr decltype(auto) base_type_v() { return type_v<base_type>; }
};
constexpr bit7 ESC{{1, 11}};
constexpr bit7 CSI{{5, 11}};
constexpr decltype(auto) control_sequence() {}
constexpr decltype(auto) control_sequence(integer<8>) {}
constexpr decltype(auto) control_sequence(integer<8>, integer<8>) {}
constexpr decltype(auto) control_sequence(integer<8>, integer<8>,
                                          integer<8>...) {
  // static_assert(sizeof...() > 0);
}
} // namespace ecma48
} // namespace gut

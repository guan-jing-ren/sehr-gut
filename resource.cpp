#include "resource.hpp"

#include <cstring>
#include <iostream>

using namespace gut;
using namespace std;

namespace gut {
constexpr directory::directory(const char *name) {
  m_resource = opendir(name);
  if (m_resource)
    operator++();
}
directory::~directory() { m_current = nullptr; }
constexpr directory::operator bool() const { return m_current; }
constexpr const dirent *directory::operator->() const { return m_current; }
constexpr const dirent &directory::operator*() const { return *m_current; }
directory &directory::operator++() {
  m_current = readdir(m_resource);
  return *this;
}

constexpr address_info::address_info(const char *node, const char *service) {
  addrinfo hint{};
  hint.ai_flags = AI_ALL | AI_CANONNAME;
  getaddrinfo(node, service, &hint, &m_resource);
  m_current = m_resource;
}
address_info::~address_info() { m_current = nullptr; }
constexpr address_info::operator bool() const { return m_current; }
constexpr const addrinfo *address_info::operator->() const { return m_current; }
constexpr const addrinfo &address_info::operator*() const { return *m_current; }
constexpr address_info &address_info::operator++() {
  m_current = m_current->ai_next;
  return *this;
}

constexpr file_glob::file_glob(const char *pattern) {
  glob(pattern, GLOB_MARK, nullptr, &m_glob);
}
constexpr decltype(auto) file_glob::count() const { return m_glob.gl_pathc; }
constexpr file_glob::operator bool() const { return count(); }
constexpr const char *const *file_glob::begin() const {
  return m_glob.gl_pathv;
}
constexpr const char *const *file_glob::end() const {
  return begin() + count();
}

constexpr word_expansion::word_expansion(const char *pattern) {
  wordexp(pattern, &m_wordexp, 0);
}
constexpr decltype(auto) word_expansion::count() const {
  return m_wordexp.we_wordc;
}
constexpr word_expansion::operator bool() const { return count(); }
constexpr const char *const *word_expansion::begin() const {
  return m_wordexp.we_wordv;
}
constexpr const char *const *word_expansion::end() const {
  return begin() + count();
}

constexpr network_interfaces::network_interfaces() {
  if ((m_end = m_resource = if_nameindex()))
    while (m_end->if_index != 0 && m_end->if_name)
      ++m_end;
}
constexpr network_interfaces::operator bool() const { return m_resource; }
constexpr const struct if_nameindex *network_interfaces::begin() const {
  return m_resource;
}
constexpr const struct if_nameindex *network_interfaces::end() const {
  return m_end;
}

constexpr group_database::group_database() {
  setgrent();
  operator++();
}
group_database::~group_database() { m_current = nullptr; }
constexpr group_database::operator bool() const { return m_current; }
constexpr const group *group_database::operator->() const { return m_current; };
constexpr const group &group_database::operator*() const { return *m_current; };
group_database &group_database::operator++() {
  m_current = getgrent();
  return *this;
}

constexpr host_database::host_database() {
  sethostent(false);
  operator++();
}
host_database::~host_database() { m_current = nullptr; }
constexpr host_database::operator bool() const { return m_current; }
constexpr const hostent *host_database::operator->() const {
  return m_current;
};
constexpr const hostent &host_database::operator*() const {
  return *m_current;
};
host_database &host_database::operator++() {
  m_current = gethostent();
  return *this;
}

constexpr network_database::network_database() {
  setnetent(false);
  operator++();
}
network_database::~network_database() { m_current = nullptr; }
constexpr network_database::operator bool() const { return m_current; }
constexpr const netent *network_database::operator->() const {
  return m_current;
};
constexpr const netent &network_database::operator*() const {
  return *m_current;
};
network_database &network_database::operator++() {
  m_current = getnetent();
  return *this;
}

constexpr protocol_database::protocol_database() {
  setprotoent(false);
  operator++();
}
protocol_database::~protocol_database() { m_current = nullptr; }
constexpr protocol_database::operator bool() const { return m_current; }
constexpr const protoent *protocol_database::operator->() const {
  return m_current;
};
constexpr const protoent &protocol_database::operator*() const {
  return *m_current;
};
protocol_database &protocol_database::operator++() {
  m_current = getprotoent();
  return *this;
}

constexpr user_database::user_database() {
  setpwent();
  operator++();
}
user_database::~user_database() { m_current = nullptr; }
constexpr user_database::operator bool() const { return m_current; }
constexpr const passwd *user_database::operator->() const { return m_current; };
constexpr const passwd &user_database::operator*() const { return *m_current; };
user_database &user_database::operator++() {
  m_current = getpwent();
  return *this;
}

constexpr service_database::service_database() {
  setservent(false);
  operator++();
}
service_database::~service_database() { m_current = nullptr; }
constexpr service_database::operator bool() const { return m_current; }
constexpr const servent *service_database::operator->() const {
  return m_current;
};
constexpr const servent &service_database::operator*() const {
  return *m_current;
};
service_database &service_database::operator++() {
  m_current = getservent();
  return *this;
}

constexpr account_database::account_database() {
  setutxent();
  operator++();
}
account_database::~account_database() { m_current = nullptr; }
constexpr account_database::operator bool() const { return m_current; }
constexpr const utmpx *account_database::operator->() const {
  return m_current;
};
constexpr const utmpx &account_database::operator*() const {
  return *m_current;
};
account_database &account_database::operator++() {
  m_current = getutxent();
  return *this;
}

} // namespace gut

int main(int c, char **v) {
  static_assert(0.0 < 0.1 * 1.1);
  for (account_database adb; adb; ++adb)
    cout << "User name: " << adb->ut_user << ", init id: " << adb->ut_id
         << ", device: " << adb->ut_line << '\n'
         << "\tProcess id: " << adb->ut_pid << ", entry type: " << adb->ut_type
         << ", time: " << adb->ut_tv.tv_sec << '.' << adb->ut_tv.tv_usec
         << '\n';
  for (group_database gdb; gdb; ++gdb) {
    cout << "Group id: " << gdb->gr_gid << ", name: " << gdb->gr_name << '\n';
    cout << "\tMembers:";
    for (auto mem = gdb->gr_mem; !mem && !*mem; ++mem)
      cout << ' ' << *mem;
    cout << '\n';
  }
  for (host_database hdb; hdb; ++hdb) {
    cout << "Host name: " << hdb->h_name
         << ", address type: " << hdb->h_addrtype
         << ", length: " << hdb->h_length << '\n';
    cout << "\tAliases:";
    for (auto alias = hdb->h_aliases; !alias && !*alias; ++alias)
      cout << ' ' << *alias;
    cout << '\n';
    cout << "\tAddresses:";
    for (auto address = hdb->h_addr_list; !address && !*address; ++address)
      cout << ' ' << *address;
    cout << '\n';
  }
  for (auto &&interface : network_interfaces{})
    cout << "Inteface index: " << interface.if_index
         << ", name: " << interface.if_name << '\n';
  for (network_database ndb; ndb; ++ndb) {
    cout << "Network number: " << ndb->n_net
         << ", address type: " << ndb->n_addrtype << ", name: " << ndb->n_name
         << '\n';
    cout << "\tAliases:";
    for (auto alias = ndb->n_aliases; !alias && !*alias; ++alias)
      cout << ' ' << *alias;
    cout << '\n';
  }
  for (protocol_database pdb; pdb; ++pdb) {
    cout << "Protocol number: " << pdb->p_proto << ", name: " << pdb->p_name
         << '\n';
    cout << "\tAliases:";
    for (auto alias = pdb->p_aliases; !alias && !*alias; ++alias)
      cout << ' ' << *alias;
    cout << '\n';
  }
  for (service_database sdb; sdb; ++sdb) {
    cout << "Service name: " << sdb->s_name << ", port: " << sdb->s_port
         << ", protocol: " << sdb->s_proto << '\n';
    cout << "\tAliases:";
    for (auto alias = sdb->s_aliases; !alias && !*alias; ++alias)
      cout << ' ' << *alias;
    cout << '\n';
  }
  for (user_database udb; udb; ++udb)
    cout << "User id: " << udb->pw_uid << ", group id: " << udb->pw_gid
         << ", name: " << udb->pw_name << '\n'
         << "\tInitial directory: " << udb->pw_dir
         << ", shell: " << udb->pw_shell << '\n';

  for (auto arg = v + 1; arg != v + c; ++arg) {
    for (address_info addr{nullptr, *arg}; addr; ++addr)
      if (addr->ai_canonname)
        cout << "Canonical name: " << addr->ai_canonname << '\n';
    for (address_info addr{*arg, nullptr}; addr; ++addr)
      if (addr->ai_canonname)
        cout << "Canonical name: " << addr->ai_canonname << '\n';
  }
  for (auto arg = v + 1; arg != v + c; ++arg) {
    for (directory dir{*arg}; dir; ++dir)
      cout << "Directory entry name: " << dir->d_name
           << ", inode: " << dir->d_ino << '\n';
  }
  cout << "Arg count: " << (c - 1) << '\n';
  for (auto arg = v + 1; arg != v + c; ++arg) {
    for (auto name : file_glob{*arg})
      cout << "File name: " << name << '\n';
  }
  for (auto arg = v + 1; arg != v + c; ++arg) {
    for (auto expansion : word_expansion{*arg})
      cout << "Word expansion: " << expansion << ", from: " << *arg << '\n';
  }

  return 0;
}
